#!/bin/sh -e

VERSION=$2
TAR=../felix-scr_$VERSION.orig.tar.gz
DIR=felix-scr-$VERSION
TAG=felix-dev-org.apache.felix.scr-$VERSION

tar -xf $TAR
unlink $TAR
cp -r $TAG/scr $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR $TAG
