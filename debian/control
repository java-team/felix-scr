Source: felix-scr
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Sudip Mukherjee <sudipm.mukherjee@gmail.com>
Build-Depends: debhelper-compat (= 13), default-jdk, maven-debian-helper,
               libosgi-compendium-java, libosgi-core-java, libfelix-gogo-runtime-java,
               libeclipse-osgi-services-java, libanimal-sniffer-java,
               libmaven-bundle-plugin-java, libmaven-javadoc-plugin-java
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/felix-scr.git
Vcs-Browser: https://salsa.debian.org/java-team/felix-scr
Homepage: https://github.com/apache/felix-dev/tree/master/scr

Package: libfelix-scr-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}, libfelix-scr-java-doc
Description: Felix Service Component Runtime
 The Felix project is an implementation of the OSGi R7 core framework
 specification.
 .
 OSGi framework is a module system and service platform for the Java
 programming language that implements a complete and dynamic component
 model.
 .
 This subproject implements the OSGi Declarative Services specification
 providing a service-oriented component model to simplify OSGi-based
 development.

Package: libfelix-scr-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${maven:DocDepends}, ${maven:DocOptionalDepends}
Suggests: libfelix-scr-java
Description: Documentation for Felix Service Component Runtime
 The Felix project is an implementation of the OSGi R7 core framework
 specification.
 .
 OSGi framework is a module system and service platform for the Java
 programming language that implements a complete and dynamic component
 model.
 .
 This subproject implements the OSGi Declarative Services specification
 providing a service-oriented component model to simplify OSGi-based
 development.
 .
 This package contains the API documentation of libfelix-scr-java.
